/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.avatarpicker.data

import com.android.avatarpicker.R
import com.android.avatarpicker.data.entity.ResourceEntity

class MediaRepository {
    fun getPhotoPicker() = ResourceEntity(
        R.drawable.ic_avatar_choose_photo, R.string.user_image_choose_photo
    )

    fun getCamera() = ResourceEntity(
        R.drawable.ic_avatar_take_photo, R.string.user_image_take_photo
    )
}